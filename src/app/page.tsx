'use client';

import Image from 'next/image';
import { useState } from 'react';
import { faEnvelope, faCode, faEyeSlash, faEye } from '@fortawesome/free-solid-svg-icons';
import {
  faFacebook,
  faGithub,
  faInstagram,
  faGitlab,
} from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Home = () => {
  const [toggle_modal_button_hidden, setToggleModalButton] = useState(true);
  const [modal_hidden, setModal] = useState(false);

  const ShowBackground = () => {
    setToggleModalButton(!toggle_modal_button_hidden);
    setModal(!modal_hidden);
  };

  return (
    <main className='flex min-h-screen flex-col items-center justify-between'>
      <video
        className='gif'
        width='100%'
        height='100%'
        // frameborder='0'
        src='/japan_train.gif'
        autoPlay
        muted
        loop
      />
      <div className={modal_hidden ? 'modal HIDE' : 'modal SHOW'}>
        <div className='main'>
          <Image
            className='img'
            src='/samurai.png'
            alt='Image of an samurai'
            width={1000}
            height={1000}
            priority={true}
          />

          <h1 className='heading'>Milijan Mosić</h1>
        </div>

        <div className='side'>
          <div className='dots'>
            <h3>Software Engineer</h3>
            <h3>Minimalist</h3>
            <h3>Nature Lover</h3>
          </div>

          <div className='links'>
            <a
              className='a'
              href='https://gitlab.com/milijan-mosic/'
              aria-label='Visit my Gitlab profile'>
              <FontAwesomeIcon icon={faGitlab} className='icon' />
            </a>
            <a
              className='a'
              href='https://www.instagram.com/windwalk.ext4/'
              aria-label='Visit my Instagram profile'>
              <FontAwesomeIcon icon={faInstagram} className='icon' />
            </a>
            <a
              className='a'
              href='mailto:milijan-mosic@protonmail.com'
              aria-label='Send me an email by visiting this link'>
              <FontAwesomeIcon icon={faEnvelope} className='icon' />
            </a>
            <a
              className='a'
              href='https://github.com/windwalk-bushido/'
              aria-label='Visit my Github profile'>
              <FontAwesomeIcon icon={faGithub} className='icon' />
            </a>
            <a
              className='a'
              href='https://www.facebook.com/legioxequestris'
              aria-label='Visit my Facebook profile'>
              <FontAwesomeIcon icon={faFacebook} className='icon' />
            </a>
            <a
              className='a'
              href='https://gitlab.com/milijan-mosic/digital-id'
              aria-label='Visit this website code repo'>
              <FontAwesomeIcon icon={faCode} className='icon' />
            </a>
          </div>

          <div className='footer'>
            <div className='div-btn-hide'>
              <button
                aria-label='Hide modal'
                className='btn_hide'
                onClick={() => ShowBackground()}>
                <FontAwesomeIcon icon={faEye} className='btn-icon' />
              </button>
            </div>

            <p className='year'>2021.</p>
          </div>
        </div>
      </div>
      <button
        className={
          toggle_modal_button_hidden
            ? 'btn-show hide-for-mobile HIDE'
            : 'btn-show hide-for-mobile SHOW'
        }
        aria-label='Show modal'
        onClick={() => ShowBackground()}>
        <FontAwesomeIcon icon={faEyeSlash} className='icon-show' />
      </button>
    </main>
  );
};

export default Home;
